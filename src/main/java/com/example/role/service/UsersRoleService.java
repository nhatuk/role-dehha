package com.example.role.service;

import com.example.role.entity.UserRoleEntity;

import java.util.List;

public interface UsersRoleService {

    List<UserRoleEntity> getAllData();
    UserRoleEntity createDat(UserRoleEntity userRole);
    void deleteUserRoleById(Integer id);
    UserRoleEntity findUserRoleByid(Integer id);
}
