package com.example.role.service;

import com.example.role.entity.RoleEntity;

import java.util.List;

public interface RoleService {

    List<RoleEntity> getAllRoles();
    RoleEntity findRoleByRoleName(String name);
    void deleteRoleByName(String name);
    RoleEntity createRole(RoleEntity role);
    RoleEntity findRoleById(Integer roleId);
}
