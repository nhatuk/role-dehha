package com.example.role.service.impl;

import com.example.role.entity.RoleEntity;
import com.example.role.repo.RoleRepository;
import com.example.role.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public List<RoleEntity> getAllRoles() {
        List<RoleEntity> roles = roleRepository.findAll();
        return roles;
    }

    @Override
    public RoleEntity findRoleByRoleName(String name) {
        return roleRepository.findRoleEntityByName(name);
    }

    @Override
    public void deleteRoleByName(String name) {
        roleRepository.deleteRoleEntityByName(name);
    }

    @Override
    public RoleEntity createRole(RoleEntity role) {
        return roleRepository.save(role);
    }

    @Override
    public RoleEntity findRoleById(Integer roleId) {
        return roleRepository.findRoleEntityById(roleId);
    }
}
