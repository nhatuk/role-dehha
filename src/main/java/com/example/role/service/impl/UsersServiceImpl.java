package com.example.role.service.impl;

import com.example.role.entity.UsersEntity;
import com.example.role.repo.UsersRepository;
import com.example.role.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    UsersRepository usersRepository;

    @Override
    public List<UsersEntity> getAllUser() {
        List<UsersEntity> users = usersRepository.findAll();
        return users;
    }

    @Override
    public UsersEntity findUserByUsername(String name) {
        return usersRepository.findUsersEntityByName(name);
    }

    @Override
    public UsersEntity createUser(UsersEntity user) {
        return usersRepository.save(user);
    }

    @Override
    public void deleteUserById(Integer userId) {
        usersRepository.deleteById(userId);
    }

    @Override
    public UsersEntity findUserByUserId(Integer userId) {
        return usersRepository.findUsersEntityById(userId);
    }
}
