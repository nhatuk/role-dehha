package com.example.role.service.impl;

import com.example.role.entity.UserRoleEntity;
import com.example.role.repo.UsersRoleRepository;
import com.example.role.service.UsersRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRoleServiceImpl implements UsersRoleService {

    @Autowired
    UsersRoleRepository usersRoleRepository;

    @Override
    public List<UserRoleEntity> getAllData() {
        List<UserRoleEntity> userRole = usersRoleRepository.findAll();
        return userRole;
    }

    @Override
    public UserRoleEntity createDat(UserRoleEntity userRole) {
        return usersRoleRepository.save(userRole);
    }

    @Override
    public void deleteUserRoleById(Integer id) {
        usersRoleRepository.deleteById(id);
    }

    @Override
    public UserRoleEntity findUserRoleByid(Integer id) {
        return usersRoleRepository.findUserRoleEntityById(id);
    }
}
