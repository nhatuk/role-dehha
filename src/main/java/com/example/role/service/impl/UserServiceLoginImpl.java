package com.example.role.service.impl;

import com.example.role.dto.UserPrincipal;
import com.example.role.entity.UsersEntity;
import com.example.role.repo.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class UserServiceLoginImpl implements UserDetailsService {

    @Autowired
    UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        UsersEntity user = usersRepository.findUsersEntityByName(name);
        if (user == null){
            throw new UsernameNotFoundException("User not found");
        }
        UserPrincipal userPrincipal = UserPrincipal.createUserPrincipal(user);
        return userPrincipal;
    }
}
