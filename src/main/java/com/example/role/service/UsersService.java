package com.example.role.service;

import com.example.role.entity.UsersEntity;

import java.util.List;

public interface UsersService {

    List<UsersEntity> getAllUser();
    UsersEntity findUserByUsername(String name);
    UsersEntity createUser(UsersEntity user);
    void deleteUserById(Integer userId);
    UsersEntity findUserByUserId(Integer userId);

}
