package com.example.role.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    public String index(){
        return "index";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/user")
    public String user(){
        return "/user/index";
    }

    @GetMapping("/role")
    public String role(){
        return "role/index";
    }
}
