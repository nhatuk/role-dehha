package com.example.role.controller.api;

import com.example.role.entity.RoleEntity;
import com.example.role.entity.UserRoleEntity;
import com.example.role.entity.UsersEntity;
import com.example.role.service.RoleService;
import com.example.role.service.UsersRoleService;
import com.example.role.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserRoleApiController {

    @Autowired
    UsersService usersService;

    @Autowired
    RoleService roleService;

    @Autowired
    UsersRoleService usersRoleService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/user-role")
    public ResponseEntity<?> getAllData(){
        List<UserRoleEntity> userRoles = usersRoleService.getAllData();;
        return new ResponseEntity<>(userRoles,HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/user-role")
    public ResponseEntity<?> updateRoleForUsers(@RequestBody UserRoleEntity userRoleEntity){
        UsersEntity users = usersService.findUserByUserId(userRoleEntity.getUserId());
        if (users == null){
            return new ResponseEntity<>("User was not exits", HttpStatus.BAD_REQUEST);
        }
        List<UsersEntity> user = new ArrayList<>();
        user.add(users);
        userRoleEntity.setUserId(userRoleEntity.getUserId());
        userRoleEntity.setUsersByUserId(users);
        RoleEntity roles = roleService.findRoleById(userRoleEntity.getRoleId());
        if (roles == null){
            return new ResponseEntity<>("Role was not exits",HttpStatus.BAD_REQUEST);
        }
        List<RoleEntity> role = new ArrayList<>();
        role.add(roles);
        userRoleEntity.setRoleId(userRoleEntity.getRoleId());
        userRoleEntity.setRoleByRoleId(roles);
        usersRoleService.createDat(userRoleEntity);
        return new ResponseEntity<>(userRoleEntity, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/user-role/{id}")
    public ResponseEntity<UserRoleEntity> deleteUserRoleById(@PathVariable("id") Integer id){
        UserRoleEntity userRole = usersRoleService.findUserRoleByid(id);
        if (userRole == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        usersRoleService.deleteUserRoleById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
