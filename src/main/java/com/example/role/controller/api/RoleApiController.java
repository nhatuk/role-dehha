package com.example.role.controller.api;

import com.example.role.entity.RoleEntity;
import com.example.role.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RoleApiController {

    @Autowired
    RoleService roleService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/role")
    public ResponseEntity<List<RoleEntity>> getListRole(){
        List<RoleEntity> roles = roleService.getAllRoles();;
        return new ResponseEntity<>(roles, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @PostMapping("/role")
    public ResponseEntity<?> createRoles(@RequestBody RoleEntity roleEntity){
        RoleEntity roles = roleService.findRoleByRoleName(roleEntity.getName());
        if (roles != null){
            return new ResponseEntity<>("Roles " + roleEntity.getName() + " was exits",HttpStatus.BAD_REQUEST);
        }
        roleService.createRole(roleEntity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/role/{id}")
    public ResponseEntity<RoleEntity> getRoleById(@PathVariable("id") Integer id){
        RoleEntity roles = roleService.findRoleById(id);
        if (roles != null){
            return new ResponseEntity<>(roles, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
