package com.example.role.controller.api;

import com.example.role.entity.RoleEntity;
import com.example.role.entity.UsersEntity;
import com.example.role.service.RoleService;
import com.example.role.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.*;

@RestController
@RequestMapping("/api")
public class UsersApiController {

    @Autowired
    UsersService usersService;

    @Autowired
    RoleService roleService;

    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * register
     *
     * @param user - UsersEntity
     * @return user
     */
    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody UsersEntity user) {
        UsersEntity users = usersService.findUserByUsername(user.getName());
        if (users != null) {
            return new ResponseEntity<>("User " + user.getName() + " was exits", HttpStatus.BAD_REQUEST);
        }
        HashSet<RoleEntity> role = new HashSet<>();
        if (roleService.findRoleByRoleName("ROLE_USER") == null) {
            roleService.createRole(new RoleEntity("ROLE_USER"));
        }
        role.add(roleService.findRoleByRoleName("ROLE_USER"));
        user.setPassword(bCryptPasswordEncoder().encode(user.getPassword()));
        user.setCreatedAt(LocalDate.now());
        user.setRoles(role);
        usersService.createUser(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @GetMapping("/user")
    public ResponseEntity<List<UsersEntity>> getAllUsers(){
        List<UsersEntity> users = usersService.getAllUser();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @GetMapping("/user/{id}")
    public ResponseEntity<UsersEntity> getUserById(@PathVariable("id") Integer id){
        UsersEntity usersEntity = usersService.findUserByUserId(id);
        if (usersEntity != null){
            return new ResponseEntity<>(usersEntity, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
