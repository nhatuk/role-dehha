package com.example.role.repo;

import com.example.role.entity.UserRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRoleRepository extends JpaRepository<UserRoleEntity, Integer> {

    UserRoleEntity findUserRoleEntityById(Integer id);
}
