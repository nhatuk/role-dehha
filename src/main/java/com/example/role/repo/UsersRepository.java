package com.example.role.repo;

import com.example.role.entity.UsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<UsersEntity, Integer> {
    UsersEntity findUsersEntityByName(String name);

    UsersEntity findUsersEntityById(Integer id);
}
