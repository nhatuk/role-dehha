package com.example.role.repo;

import com.example.role.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {

    RoleEntity findRoleEntityByName(String name);

    void deleteRoleEntityByName(String name);

    RoleEntity findRoleEntityById(Integer id);
}
