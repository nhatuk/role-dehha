package com.example.role.dto;

import com.example.role.entity.RoleEntity;
import com.example.role.entity.UsersEntity;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@Data
@AllArgsConstructor
public class UserPrincipal implements UserDetails {

    private UsersEntity user;

    private Integer id;
    private String name;
    private String password;
    private Collection<? extends GrantedAuthority> roles;
    private Set<GrantedAuthority> authorities;

    public UserPrincipal(UsersEntity user) {
        this.user = user;
    }

    /**
     * createUserPrincipal
     *
     * @param user - UsersEntity
     * @return UserPrincipal
     */
    public static UserPrincipal createUserPrincipal(UsersEntity user) {
        if (user != null) {
            List<GrantedAuthority> roles = user.getRoles().stream().filter(Objects::nonNull)
                    .map(role -> new SimpleGrantedAuthority(role.getName()))
                    .collect(Collectors.toList());

            Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
            Collection<RoleEntity> b = user.getRoles();
            for (RoleEntity role : b) {
                grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
            }

            return UserPrincipal.builder()
                    .user(user)
                    .id(user.getId())
                    .name(user.getName())
                    .password(user.getPassword())
                    .roles(roles)
                    .authorities(grantedAuthorities)
                    .build();
        }
        return null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
